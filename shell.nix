with import <nixpkgs> {};

mkShell {
  buildInputs = [ rustc cargo ffmpeg rls ];
}
